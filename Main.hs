-- Advanced Programming, HW 4
-- by <David Xu>  <78152170>
--    <Suiyao Ma> <81848688>

{-# OPTIONS -Wall -fno-warn-unused-binds -fno-warn-unused-matches -fwarn-tabs -fno-warn-type-defaults #-}
{-# LANGUAGE NoImplicitPrelude, FlexibleInstances, TemplateHaskell #-}

module Main where

import Prelude hiding (mapM,any,all,filter,sequence)


import Data.Map (Map)
import qualified Data.Map as Map

import Data.Set (Set)
import qualified Data.Set as Set

import Control.Monad.State hiding (when, mapM, foldM, (>=>), sequence, join, liftM, liftM2)

import Test.HUnit hiding (State)
import Test.QuickCheck
import Test.QuickCheck.Function


import Data.Maybe
import Data.List


main :: IO ()
main =
  do sequence_ [ qc1, qc2, qc3, qc4, qc5, qc6, qc7, qc8, qc9, qc10, qc11, -- 0
                 runTest testSearch, qc12,                                -- 1
                 qc13, qc14, runTest t4a, runTest t4b,                    -- 2
                 runTest testAccept, runTest testPat, runTest testMatch ] -- 3

runTest   :: Test -> IO ()
runTest t = runTestTT t >> return ()

-------------------------------------------------------------------------
-- Problem 0

-- (a) Define a monadic generalization of map

mapM      :: Monad m => (a -> m b) -> [a] -> m [b]
mapM f xs = sequence $ map f xs


-- (b) Define a monadic generalization of foldl

foldM            :: Monad m => (a -> b -> m a) -> a -> [b] -> m a
foldM _ a []     = return a
foldM f a (x:xs) = f a x >>= \a' -> foldM f a' xs


-- (c) Define a generalization of monadic sequencing that evaluates
-- each action in a list from left to right, collecting the results
-- in a list.

sequence :: Monad m => [m a] -> m [a]
sequence = foldr f $ return []
  where f x' a' = do x <- x'
                     a <- a'
                     return $ x : a

-- (d) Define the "fish operator", a variant of composition

(>=>)       :: Monad m => (a -> m b) -> (b -> m c) -> (a -> m c)
(>=>) f g a = do b <- f a
                 g b


-- (e) Define the 'join' operator, which removes one level of
-- monadic structure, projecting its bound argument into the outer level.

join   :: (Monad m) => m (m a) -> m a
join a = a >>= id

-- (f) Define the 'liftM' function

liftM      :: (Monad m) => (a -> b) -> m a -> m b
liftM f a' = do a <- a'
                return $ f a

liftM2         :: (Monad m) => (a -> b -> r) -> m a -> m b -> m r
liftM2 f a' b' = do a <- a'
                    b <- b'
                    return $ f a b

-------------------------------------------------------------------------
-- Problem 1

data AList a = Nil | Single a | Append (AList a) (AList a) deriving (Show, Eq)

-- (a)

instance Functor AList where
   fmap f Nil          = Nil
   fmap f (Single a)   = Single $ f a
   fmap f (Append l r) = Append (fmap f l) (fmap f r)

instance Monad AList where
   return = Single

   Nil          >>= f = Nil
   (Single a)   >>= f = f a
   (Append l r) >>= f = Append (l >>= f) (r >>= f)

prop_FMapId :: (Eq (f a), Functor f) => f a -> Bool
prop_FMapId x = fmap id x == id x

prop_FMapComp :: (Eq (f c), Functor f) => Fun b c -> Fun a b -> f a -> Bool
prop_FMapComp (Fun _ f) (Fun _ g) x =
   fmap (f . g) x == (fmap f . fmap g) x

prop_LeftUnit :: (Eq (m b), Monad m) => a -> Fun a (m b) -> Bool
prop_LeftUnit x (Fun _ f) =
   (return x >>= f) == f x

prop_RightUnit :: (Eq (m b), Monad m) => m b -> Bool
prop_RightUnit m =
   (m >>= return) == m

prop_Assoc :: (Eq (m c), Monad m) =>
    m a -> Fun a (m b) -> Fun b (m c) -> Bool
prop_Assoc m (Fun _ f) (Fun _ g) =
   ((m >>= f) >>= g) == (m >>= \x -> f x >>= g)

prop_FunctorMonad :: (Eq (m b), Functor m, Monad m) => m a -> Fun a b -> Bool
prop_FunctorMonad x (Fun _ f) = (fmap f x) == (x >>= return . f)

instance Arbitrary a => Arbitrary (AList a) where
   arbitrary = frequency [ (1, return Nil)
                         , (3, liftM Single arbitrary)
                         , (3, liftM2 Append arbitrary arbitrary) ]

   shrink Nil          = []
   shrink (Single a)   = [Nil]
   shrink (Append l r) = [Append l Nil, Append Nil r, Append Nil Nil]



qc1 :: IO ()
qc1 = quickCheck (prop_FMapId  :: AList Int -> Bool)

qc2 :: IO ()
qc2 = quickCheck (prop_FMapComp :: Fun Int Int -> Fun Int Int -> AList Int
                                -> Bool)

qc3 :: IO ()
qc3 = quickCheck (prop_LeftUnit  :: Int -> Fun Int (AList Int) -> Bool)

qc4 :: IO ()
qc4 = quickCheck (prop_RightUnit :: AList Int -> Bool)

qc5 :: IO ()
qc5 = quickCheck (prop_Assoc :: AList Int -> Fun Int (AList Int)
                             -> Fun Int (AList Int) -> Bool)

qc6 :: IO ()
qc6 = quickCheck (prop_FunctorMonad :: AList Int -> Fun Int (AList Int)
                                    -> Bool)


-- List equality tests
prop_fmapListEquality             :: Eq a => Fun a (AList a) -> AList a -> Bool
prop_fmapListEquality (Fun _ f) s = toList (fmap f s) == fmap f (toList s)

prop_returnListEquality :: Eq a => a -> Bool
prop_returnListEquality x = toList (return x) == return x

prop_bindListEquality             :: Eq b => AList a -> (Fun a (AList b))
                                  -> Bool
prop_bindListEquality m (Fun _ k) =
  (toList (m >>= k)) == (toList m >>= (toList . k))


qc7 :: IO ()
qc7 = quickCheck (prop_fmapListEquality :: Fun Int (AList Int) -> AList Int
                                        -> Bool)


qc8 :: IO ()
qc8 = quickCheck (prop_returnListEquality :: AList Int -> Bool)

qc9 :: IO ()
qc9 = quickCheck (prop_bindListEquality :: AList Int -> (Fun Int (AList Char))
                                        -> Bool)

-- (b)

{- Invalid instance of Functor and Monad:

The following AList instance of Functor fails the following properties whenever
the `AList` is not `Nil`:
 - identity
 - composition

instance Functor AList where
   fmap _ Nil          = Nil


The following AList of Monad fails the following properties whenever the
`AList`(s) is/are not `Nil`:
 - left identity
 - right identity
 - associativity

instance Monad AList where
   return = Single

   _ >>= f = Nil

-}



-- (c)

-- | access the first element of the list, if there is one.
first              :: AList a -> Maybe a
first Nil          = Nothing
first (Single a)   = Just a
first (Append l r) = first l `mplus` first r

prop_first   :: Eq a => AList a -> Bool
prop_first a = (listToMaybe $ toList a) == first a

qc10 :: IO ()
qc10 = quickCheck (prop_first :: AList Int -> Bool)



-- | access the last element of the list, if there is one
final              :: AList a -> Maybe a
final Nil          = Nothing
final (Single a)   = Just a
final (Append l r) = final r `mplus` final l

prop_final   :: Eq a => AList a -> Bool
prop_final a = (listToMaybe $ (reverse . toList) a) == final a

qc11 :: IO ()
qc11 = quickCheck (prop_final :: AList Int -> Bool)

-- (d)

instance MonadPlus AList where
  mzero = Nil
  mplus = Append

search :: (MonadPlus m) => (a -> Bool) -> AList a -> m a
search p Nil          = mzero
search p (Single a)
  | p a       = return a
  | otherwise = mzero
search p (Append l r) = search p l `mplus` search p r

-- N.b.: Do not change the type signatures or definitions of `findAny`,
-- `findAll`, or -- `prune`.  They must be exactly as we give them here.

findAny :: (a -> Bool) -> AList a -> Maybe a
findAny = search

findAll :: (a -> Bool) -> AList a -> [a]
findAll = search

prune :: (a -> Bool) -> AList a -> AList a
prune = search

testSearch :: Test
testSearch = TestList [ "search1" ~: findAny (>3)    seq1 ~=? Just 4
                      , "search2" ~: findAll (>3)    seq1 ~=? [4,5]
                      , "search3" ~: prune   (>3)    seq1 ~=?
                                  Append (Append Nil Nil)
                                           (Append (Single 4) (Single 5)) ]

seq1 :: AList Int
seq1 = Append (Append Nil (Single 3)) (Append (Single 4) (Single 5))

-- (e)

toList :: AList a -> [a]
toList Nil = []
toList (Single x)     = [x]
toList (Append s1 s2) = toList s1 ++ toList s2

showAsList :: Show a => AList a -> String
showAsList = show . toList

exAList :: AList Int
exAList = Append (Append (Append (Append (Single 0)
                                         (Single 1))
                                 (Single 2))
                        (Single 3))
                 (Single 4)

testToList :: Test
testToList = toList exAList ~?= [0,1,2,3,4]

toListFast   :: AList a -> [a]
toListFast a = toListFast' a []
  where toListFast' Nil xs          = xs
        toListFast' (Single x) xs   = x : xs
        toListFast' (Append l r) xs = toListFast' l $ toListFast' r xs

prop_toList   :: Eq a => AList a -> Bool
prop_toList l = toListFast l == toList l

qc12 :: IO ()
qc12 = quickCheck (prop_toList :: AList Int -> Bool)

-------------------------------------------------------------------------
-- Problem 2

type Variable = String

data Statement =
    Assign Variable Expression          -- x = e
  | Skip                                -- no-op
  | Sequence Statement Statement        -- s1; s2
  | If Expression Statement Statement   -- if (e) {s1} else {s2}
  | While Expression Statement          -- while (e) {s}
  deriving (Eq, Show)

data Expression =
    Var Variable                        -- x
  | Val Value                           -- v
  | Op  Bop Expression Expression
  deriving (Eq, Show)

data Bop =
    Plus     -- +  :: Int -> Int -> Int
  | Minus    -- -  :: Int -> Int -> Int
  | Times    -- *  :: Int -> Int -> Int
  | Divide   -- /  :: Int -> Int -> Int
  | Gt       -- >  :: Int -> Int -> Bool
  | Ge       -- >= :: Int -> Int -> Bool
  | Lt       -- <  :: Int -> Int -> Bool
  | Le       -- <= :: Int -> Int -> Bool
  deriving (Eq, Show)

data Value =
    IntVal Int
  | BoolVal Bool
  deriving (Eq, Show)

type Store = Map Variable Value

evalBop                                :: Bop -> Value -> Value -> Value
evalBop Plus   (IntVal v1) (IntVal v2) = IntVal (v1 + v2)
evalBop Times  (IntVal v1) (IntVal v2) = IntVal (v1 * v2)
evalBop Minus  (IntVal v1) (IntVal v2) = IntVal (v1 - v2)
evalBop Divide (IntVal v1) (IntVal v2) = IntVal (v1 `quot` v2)
evalBop Gt     (IntVal v1) (IntVal v2) = BoolVal (v1 > v2)
evalBop Ge     (IntVal v1) (IntVal v2) = BoolVal (v1 >= v2)
evalBop Lt     (IntVal v1) (IntVal v2) = BoolVal (v1 < v2)
evalBop Le     (IntVal v1) (IntVal v2) = BoolVal (v1 <= v2)
evalBop _     _           _            = IntVal 0


evalE                :: Expression -> State Store Value
evalE (Var v)        = do s <- get
                          return $ s Map.! v
evalE (Val v)        = return v
evalE (Op bop e1 e2) = do v1 <- evalE e1
                          v2 <- evalE e2
                          return $ evalBop bop v1 v2


evalS :: Statement -> State Store ()
evalS (While e s)      = do BoolVal b <- evalE e
                            if b
                            then do evalS s
                                    evalS $ While e s
                            else return ()
evalS Skip             = return ()
evalS (Sequence s1 s2) = do evalS s1
                            evalS s2
evalS (Assign x e)     = do v <- evalE e
                            m <- get
                            put $ Map.insert x v m

evalS (If e s1 s2)     = do BoolVal b <- evalE e
                            if b
                            then evalS s1
                            else evalS s2

execS :: Statement -> Store -> Store
execS = execState . evalS

run :: Statement -> IO ()
run stmt = do putStrLn "Output Store:"
              print (execS stmt Map.empty)

wTest :: Statement
wTest = Sequence (Assign "X" (Op Plus (Op Minus (Op Plus (Val (IntVal 1)) (Val (IntVal 2))) (Val (IntVal 3))) (Op Plus (Val (IntVal 1)) (Val (IntVal 3))))) (Sequence (Assign "Y" (Val (IntVal 0))) (While (Op Gt (Var "X") (Val (IntVal 0))) (Sequence (Assign "Y" (Op Plus (Var "Y") (Var "X"))) (Assign "X" (Op Minus (Var "X") (Val (IntVal 1)))))))

wFact :: Statement
wFact = Sequence (Assign "N" (Val (IntVal 5))) (Sequence (Assign "F" (Val (IntVal 1))) (While (Op Gt (Var "N") (Val (IntVal 0))) (Sequence (Assign "X" (Var "N")) (Sequence (Assign "Z" (Var "F")) (Sequence (While (Op Gt (Var "X") (Val (IntVal 1))) (Sequence (Assign "F" (Op Plus (Var "Z") (Var "F"))) (Assign "X" (Op Minus (Var "X") (Val (IntVal 1)))))) (Assign "N" (Op Minus (Var "N") (Val (IntVal 1)))))))))

t4a :: Test
t4a = execS wTest Map.empty ~?=
        Map.fromList [("X",IntVal 0),("Y",IntVal 10)]

t4b :: Test
t4b = execS wFact Map.empty ~?=
        Map.fromList [("F",IntVal 120),("N",IntVal 0),("X",IntVal 1),("Z",IntVal 120)]

-------------------------------------------------------------------------
-- Problem 3

data RegExp = Char (Set Char)      -- single literal character
            | Alt RegExp RegExp    -- r1 | r2   (alternation)
            | Seq RegExp RegExp    -- r1 r2     (concatenation)
            | Star RegExp          -- r*        (Kleene star)
            | Empty                -- ε, accepts empty string
            | Void                 -- ∅, always fails
            | Mark RegExp          -- (for marked subexpressions, see (b) below)
  deriving Show

char :: Char -> RegExp
char = Char . Set.singleton

chars :: [Char] -> RegExp
chars = Char . Set.fromList

lower, upper, letter, digit, punc, white, anyc, anyc':: RegExp
lower  = chars ['a' .. 'z']
upper  = chars ['A' .. 'Z']
digit  = chars ['0' .. '9']
punc   = chars "<>!/.*()?@"
white  = chars " \n\r\t"

anyc'  = lower `Alt` upper `Alt` digit `Alt` punc `Alt` white

anyc = chars $ ['a' .. 'z']
               ++ ['A' .. 'Z']
               ++ ['0' .. '9']
               ++ "<>!/.*()?@"
               ++ "\n\r\t"

letter = chars $ ['A' .. 'Z'] ++ ['a' .. 'z']

word  :: String -> RegExp
word w = foldr Seq Empty (map char w)

cis552 :: RegExp
cis552 = word "cis552"

boldHtml :: RegExp
boldHtml = word "<b>" `Seq` Star anyc `Seq`  word "</b>"

plus :: RegExp -> RegExp
plus pat = pat `Seq` Star pat

-- (a)

-- all decompositions of a string into two different pieces
--     split "abc" == [("","abc"),("a","bc"),("ab","c"),("abc","")]


split :: [a] -> [([a], [a])]
split = split' []
  where split' l []       = [(l, [])]
        split' l r@(y:ys) = (l, r) : split' (l ++ [y]) ys
  -- map (uncurry splitAt) $ zip [0..] $ replicate (length xs + 1) xs

-- all decompositions of a string into multi-part (nonempty) pieces
-- parts "abc" = [["abc"],["a","bc"], ["ab","c"], ["a","b","c"]]
parts    :: [a] -> [[[a]]]
parts [] = []
parts s  = foldr f [[]] s
     where f x = foldr g []
             where g []       zs = [[x]] : zs
                   g l@(y:ys) zs = ((x : y) : ys) : (([x] : l) : zs)

prop_partsLength   :: [a] -> Property
prop_partsLength s = let n = length s
                         ps = parts s
                     in  n <= 17 ==>
                         if (n == 0)
                         then 0 == length ps
                         else 2 ^ (n - 1) == length ps

prop_partsDistinct   :: Eq a => [a] -> Property
prop_partsDistinct s = let ps = nub $ parts s
                       in  length s <= 10 ==>
                           length ps == length (nub ps)

qc13 :: IO ()
qc13 = quickCheck (prop_partsLength :: String -> Property)

qc14 :: IO ()
qc14 = quickCheck (prop_partsDistinct :: String -> Property)

accept :: RegExp -> String -> Bool
accept (Mark r)    s = accept r s
accept (Alt r1 r2) s = accept r1 s || accept r2 s
accept (Char cs) [c] = Set.member c cs
accept (Char cs)   _ = False
accept (Seq r1 r2) s = any matches $ split s
  where matches (s1, s2) = accept r1 s1 && accept r2 s2
accept (Star _)   [] = True
accept (Star r)    s = let r' = Alt r (Seq r r') in accept r' s
accept Empty       s = null s
accept Void        _ = False

testAccept :: Test
testAccept =
  TestList
  [ not (accept Void "a") ~? "nothing is void"
  , not (accept Void "") ~? "really, nothing is void"
  , accept Empty "" ~? "accept Empty true"
  , not (accept Empty "a") ~? "not accept Empty"
  , accept lower "a" ~? "accept lower"
  , not (accept lower "A") ~? "not accept lower"
  , accept boldHtml "<b>cis552</b>!</b>" ~? "cis552!"
  , not (accept boldHtml "<b>cis552</b>!</b") ~? "no trailing" ]

-- (b)

-- | Mark a subexpression
-- this function just wraps the data constructor for now -- included for future
-- extensibility, see (d) below
mark :: RegExp -> RegExp
mark = Mark

boldHtmlPat :: RegExp
boldHtmlPat = word "<b>" `Seq` mark (Star anyc) `Seq` word "</b>"

namePat :: RegExp
namePat = mark (plus letter) `Seq` Star white `Seq` mark (plus letter)

wordsPat :: RegExp
wordsPat = Star (mark (plus lower) `Seq` Star white)

testPat :: Test
testPat =
  TestList
  [ patAccept boldHtmlPat "<b>cis552" ~?= Nothing
  , patAccept boldHtmlPat "<b>cis552!</b>" ~?= Just ["cis552!"]
  , patAccept boldHtmlPat "<b>cis552</b>!</b>" ~?= Just ["cis552</b>!"]
  , patAccept namePat "Haskell  Curry" ~?= Just ["Haskell", "Curry"]
  , patAccept wordsPat "a    b c   d e" ~?= Just ["a", "b", "c", "d", "e"] ]

patAccept :: RegExp -> String -> Maybe [String]
patAccept = patAccept' False
  where patAccept' _ (Mark r)    s = do res <- patAccept' True r s
                                        return res

        patAccept' q (Alt r1 r2) s = patAccept' q r1 s `mplus` patAccept' q r2 s

        patAccept' q (Char cs) [c]
          | Set.member c cs = Just $ if q then [[c]] else []
          | otherwise       = Nothing

        patAccept' q (Char cs)   _ = Nothing

        patAccept' q (Seq r1 r2) s = foldr f Nothing $ split s
          where f (s1, s2) res = res
                                 `mplus`
                                 do res1 <- (patAccept' False r1 s1)
                                    res2 <- (patAccept' False r2 s2)
                                    let res' = res1 ++ res2
                                    if q
                                    then return $ s : res'
                                    else return res'

        patAccept' _ (Star _)   [] = Just []

        patAccept' q (Star r)    s = let r' = Alt r (Seq r r')
                                     in  patAccept' q r' s

        patAccept' q Empty      [] = Just $ if q then [""] else []
        patAccept' _ Empty       _ = Nothing
        patAccept' _ Void        _ = Nothing


-- (c)

match     :: RegExp -> String -> Bool
match r s = nullable (foldl deriv r s)

-- | `nullable r` return `True` when `r` matches the empty string
nullable             :: RegExp -> Bool
nullable Empty       = True
nullable (Star _)    = True
nullable (Alt r1 r2) = nullable r1 || nullable r2
nullable (Seq r1 r2) = nullable r1 && nullable r2
nullable _           = False

-- |  Takes a regular expression `r` and a character `c`,
-- and computes a new regular expression that accepts word `w` if `cw` is
-- accepted by `r`.
deriv :: RegExp -> Char -> RegExp
deriv (Mark r)    c = deriv r c
deriv (Alt r1 r2) c = deriv r1 c `Alt` deriv r2 c
deriv (Char cs)   c
  | Set.member c cs = Empty
  | otherwise       = Void
deriv (Seq r1 r2) c = (deriv r1 c `Seq` r2) `Alt` (nu r1 `Seq` deriv r2 c)
  where nu r
          | nullable r = Empty
          | otherwise  = Void

deriv rr@(Star r) c = deriv r c `Seq` rr
deriv Empty       c = Void
deriv Void        c = Void

testMatch :: Test
testMatch =
  TestList
  [ not (match Void "a")                     ~? "nothing is void"
  , not (match Void "")                      ~? "really, nothing is void"
  , match Empty ""                           ~? "match Empty true"
  , not (match Empty "a")                    ~? "not match Empty"
  , match lower "a"                          ~? "match lower"
  , not (match lower "A")                    ~? "not match lower"
  , match boldHtml "<b>cis552</b>!</b>"      ~? "cis552!"
  , not (match boldHtml "<b>cis552</b>!</b") ~? "no trailing"
  , match boldHtmlPat "<b>cis552!</b>"       ~? "<b>cis552!</b>"
  , match boldHtmlPat "<b>cis552</b>!</b>"   ~? "<b>cis552</b>!</b>"
  , match namePat "Haskell  Curry"           ~? "Haskell  Curry"
  , match wordsPat "a    b c   d e"          ~? "a    b c   d e" ]
